package com.itbac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/4/26 23:32
 */
@SpringBootApplication
//nacos客户端服务发现
@EnableDiscoveryClient
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class);
    }
}
