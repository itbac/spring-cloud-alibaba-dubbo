package com.itbac.controller;

import com.itbac.order.IOrderService;
import com.itbac.product.IProductService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/4/26 23:42
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private IOrderService orderService;

    @Reference
    private IProductService productService;


    // http:127.0.0.1:8091/order/getOrderMsg
    @RequestMapping("/getOrderMsg")
    public String getOrderMsg(){
        String product = productService.getProduct();
        String order = orderService.getOrderStr("aaa");
        return order + "---------" + product;
    }

}
