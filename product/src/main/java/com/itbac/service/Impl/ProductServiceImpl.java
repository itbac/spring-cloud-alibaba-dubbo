package com.itbac.service.Impl;

import com.itbac.product.IProductService;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/4/26 23:40
 */
@Service
public class ProductServiceImpl implements IProductService {


    @Override
    public String getProduct() {
        return "我是一个商品";
    }

}
